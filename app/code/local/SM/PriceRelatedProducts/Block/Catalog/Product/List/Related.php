<?php
class SM_PriceRelatedProducts_Block_Catalog_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{
    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */

        $this->_itemCollection = $product->getRelatedProductCollection()
            ->addAttributeToSelect('required_options')
            ->setPositionOrder()
            ->addStoreFilter()
        ;

        if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
                Mage::getSingleton('checkout/session')->getQuoteId()
            );
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
//        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);

        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }
        if($this->getSize() == 0){
            $current_product_price = $product->getPrice();
            $config_price = Mage::getStoreConfig('pricerelatedproducts/general/price');
            $min_price = $current_product_price - $config_price;
            if($min_price < 0){
                $min_price = 0;
            }
            $max_price = $current_product_price + $config_price;
            $this->_itemCollection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('price',
                     array(
                         array('from' => $min_price, 'to' => $max_price)
                     )
                 )
                ->addAttributeToFilter('sku', array('neq' => $product->getSku()));
            Mage::getSingleton('catalog/product_status')
                ->addVisibleFilterToCollection($this->_itemCollection);

            Mage::getSingleton('catalog/product_visibility')
                ->addVisibleInCatalogFilterToCollection($this->_itemCollection);
            $this->_itemCollection->addAttributeToSort('created_at', 'desc');
            $this->_itemCollection->setPageSize(10);
            $this->_itemCollection->load();

            return $this;
        }else{
            return $this;
        }
    }
}
			