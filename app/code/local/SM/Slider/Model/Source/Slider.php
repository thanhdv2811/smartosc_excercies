<?php
class SM_Slider_Model_Source_Slider extends Mage_Eav_Model_Entity_Attribute_Source_Abstract{
    public function getAllOptions(){
        $slider_collection = Mage::getModel('slider/slider')->getCollection();
        $options = array();
        $options[0] = 'Select slider';
        foreach ($slider_collection as $slider) {
            $options[$slider->getData('slider_id')] = $slider->getData('title');
        }
        return $options;
    }
    public function toOptionArray(){
        $slider_collection = Mage::getModel('slider/slider')->getCollection();
        $options = array();
        $options[] = array(
            'label' => 'Select slider',
            'value' => '0'
        );
        foreach ($slider_collection as $slider) {
            $options[] = array(
                'label' => $slider->getData('title'),
                'value' => $slider->getData('slider_id')
            );
        }

        return $options;
    }
}