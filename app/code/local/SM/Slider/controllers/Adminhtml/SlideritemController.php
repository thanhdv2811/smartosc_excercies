<?php
class SM_Slider_Adminhtml_SlideritemController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sm_slider/slideritem')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('slider/slideritem')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('slideritem_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('sm_slider/slideritem');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('slider/adminhtml_slideritem_edit'))
                    ->_addLeft($this->getLayout()->createBlock('slider/adminhtml_slideritem_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('slider/slideritem');
            if (isset($data['image']['delete'])) {
                Mage::helper('slider')->deleteImageFile($data['image']['value']);
            }
            $banner_image_path = Mage::getBaseDir('media') . DS . 'sm_slider';
            if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                try {
                    $uploader = new Varien_File_Uploader('image');
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(true);

                    $uploader->setFilesDispersion(false);

                    $image = $uploader->save($banner_image_path, $_FILES['image']['name']);

                } catch (Exception $e) {
                    Mage::log($e->getMessage());
                }
            }
            if ($image || (isset($data['image']['delete']) && $data['image']['delete'])) {
                if($image['file'] == null)
                    $data['image'] = '';
                else
                    $data['image'] = $image['file'];
            } else {
                unset($data['image']);
            }
            foreach ($data as $key => $value)
            {
                if (is_array($value))
                {
                    if($key == 'stores' && in_array('0',$data[$key])){
                        $data[$key] = '0';
                    }
                    else{
                        $data[$key] = implode(',',$this->getRequest()->getParam($key));
                    }
                }
            }

			$model->setData($data);

            if($this->getRequest()->getParam('id')){
				$model->setData('slider_item_id', $this->getRequest()->getParam('id'));
			}
            //echo '<pre>';
            //var_dump($model);die;
            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('slider')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('slider')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function massDeleteAction() {
        $itemIds = $this->getRequest()->getParam('slider');
        if (!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($itemIds as $itemId) {
                    $slider_item = Mage::getModel('slider/slideritem')->load($itemId);
                    $slider_item->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($itemIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
}