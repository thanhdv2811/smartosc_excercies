<?php
class SM_Slider_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function deleteImageFile($image) {
        if (!$image) {
            return;
        }
        $banner_image_path = Mage::getBaseDir('media') . DS . $image;
        if (!file_exists($banner_image_path)) {
            return;
        }

        try {
            unlink($banner_image_path);
        } catch (Exception $exc) {
            Mage::log($exc->getMessage());
        }
    }

}
	 