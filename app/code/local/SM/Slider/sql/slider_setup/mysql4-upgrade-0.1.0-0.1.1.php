<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->removeAttribute('catalog_category',category_slide);
$setup->addAttribute('catalog_category', 'category_slide', array(
    'group'         => 'Custom Design',
    'input'         => 'select',
    'type'          => 'varchar',
    'label'         => 'Slider',
    'backend'        => 'eav/entity_attribute_backend_array',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'source'        => 'slider/source_slider'
));

$setup->endSetup();