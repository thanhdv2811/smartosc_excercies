<?php
class SM_Slider_Block_Widget extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getSlider(){
        $slider_id = $this->getSliderId();
        $slider_item_collection = Mage::getModel('slider/slideritem')->getCollection();
        $slider_item_collection->addFieldToFilter('status',array('eq' => 0));
        $slider_item_collection->addFieldToFilter('slider_id',array('finset' => $slider_id));
        $slider_item_collection->setOrder('sort_order','asc');
        $slider_item_collection->load();

        return $slider_item_collection;
    }

    public function getTitleSlider(){
        $slider_model = Mage::getModel('slider/slider')->load($this->getSliderId());
        if($slider_model->getData('show_title') == 0){
            return $slider_model->getData('title');
        }else{
            return false;
        }
    }
}