<?php
class SM_Slider_Block_Slider extends Mage_Core_Block_Template{

    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getCurrentSliderCategory(){
        $_current_category = Mage::registry('current_category');
        $category_model = Mage::getModel('catalog/category')->load($_current_category->getId());
        $slider_id = $category_model->getCategorySlide();
        if($slider_id == 0){
            return false;
        }
        $slider_item_collection = Mage::getModel('slider/slideritem')->getCollection();
        $slider_item_collection->addFieldToFilter('status',array('eq' => 0));
        $slider_item_collection->addFieldToFilter('slider_id',array('finset' => $slider_id));
        $slider_item_collection->setOrder('sort_order','asc');
        $slider_item_collection->load();

        return $slider_item_collection;
    }

    public function getTitleSlider(){
        $_current_category = Mage::registry('current_category');
        $category_model = Mage::getModel('catalog/category')->load($_current_category->getId());
        $slider_id = $category_model->getCategorySlide();
        $slider_model = Mage::getModel('slider/slider')->load($slider_id);
        if($slider_model->getData('show_title') == 0){
            return $slider_model->getData('title');
        }else{
            return false;
        }
    }

    public function getHomeSlider(){
        $home_slider_id = Mage::getStoreConfig('slider/general/home_page_slider');
        if($home_slider_id == 0){
            return false;
        }
        $slider_item_collection = Mage::getModel('slider/slideritem')->getCollection();
        $slider_item_collection->addFieldToFilter('status',array('eq' => 0));
        $slider_item_collection->addFieldToFilter('slider_id',array('finset' => $home_slider_id));
        $slider_item_collection->setOrder('sort_order','asc');
        $slider_item_collection->load();

        return $slider_item_collection;
    }

    public function getTitleHomeSlider(){
        $slider_model = Mage::getModel('slider/slider')->load(Mage::getStoreConfig('slider/general/home_page_slider'));
        if($slider_model->getData('show_title') == 0){
            return $slider_model->getData('title');
        }else{
            return false;
        }
    }
}