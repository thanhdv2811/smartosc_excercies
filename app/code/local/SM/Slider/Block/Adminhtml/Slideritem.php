<?php
class SM_Slider_Block_Adminhtml_Slideritem extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_slideritem';
		$this->_blockGroup = 'slider';
		$this->_headerText = Mage::helper('slider')->__('Items Manager');
		$this->_addButtonLabel = Mage::helper('slider')->__('Add Item');
		parent::__construct();
	}
}