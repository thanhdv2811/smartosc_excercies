<?php
class SM_Slider_Block_Adminhtml_Slider_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getSliderData()) {
            $data = Mage::getSingleton('adminhtml/session')->getSliderData();
            Mage::getSingleton('adminhtml/session')->setSliderData(null);
        } elseif (Mage::registry('slider_data'))
            $data = Mage::registry('slider_data')->getData();

        $fieldset = $form->addFieldset('slider_form', array('legend' => Mage::helper('slider')->__('Information')));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('slider')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('show_title', 'select', array(
            'label' => Mage::helper('slider')->__('Show Title'),
            'name' => 'show_title',
            'values' => array(
                array(
                    'value' => 0,
                    'label' => Mage::helper('slider')->__('Enabled'),
                ),
                array(
                    'value' => 1,
                    'label' => Mage::helper('slider')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('slider')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 0,
                    'label' => Mage::helper('slider')->__('Enabled'),
                ),
                array(
                    'value' => 1,
                    'label' => Mage::helper('slider')->__('Disabled'),
                ),
            ),
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }

}