<?php
class SM_Slider_Block_Adminhtml_Slideritem_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'slider';
		$this->_controller = 'adminhtml_slideritem';

		$this->_updateButton('save', 'label', Mage::helper('slider')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('slider')->__('Delete Item'));

		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

        $this->_formScripts[] = "

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

	}

	public function getHeaderText(){
		if(Mage::registry('slideritem_data') && Mage::registry('slideritem_data')->getId())
			return Mage::helper('slider')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('slideritem_data')->getName()));
		return Mage::helper('slider')->__('Add Item');
	}
}