<?php

class SM_Slider_Block_Adminhtml_Slideritem_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $dataObj = new Varien_Object(
                array(
            'store_id' => '',
            'name_in_store' => '',
            'status_in_store' => '',
            'click_url_in_store' => '',
                )
        );
        if (Mage::getSingleton('adminhtml/session')->getSlideritemData()) {
            $data = Mage::getSingleton('adminhtml/session')->getSlideritemData();
            Mage::getSingleton('adminhtml/session')->setSlideritemData(null);
        } elseif (Mage::registry('slideritem_data'))
            $data = Mage::registry('slideritem_data')->getData();

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $wysiwygConfig->addData(array(
            'add_variables' => false,
            'plugins' => array(),
            'widget_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'directives_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted' => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
        ));

        $fieldset = $form->addFieldset('item_form', array('legend' => Mage::helper('slider')->__('Item information')));

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('slider')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name'
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('slider')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 0,
                    'label' => Mage::helper('slider')->__('Enabled'),
                ),
                array(
                    'value' => 1,
                    'label' => Mage::helper('slider')->__('Disabled'),
                ),
            )
        ));

        $fieldset->addField('alt', 'text', array(
            'label' => Mage::helper('slider')->__('Alt Text'),
            'name' => 'alt',
            'note' => 'Used for SEO',
        ));

        $fieldset->addField('url', 'text', array(
            'label' => Mage::helper('slider')->__('URL'),
            'required' => false,
            'name' => 'url'
        ));

        if (isset($data['image']) && $data['image'] ) {
            $data['image'] =  'sm_slider/'.$data['image'];
        }
        $fieldset->addField('image', 'image', array(
            'label' => Mage::helper('slider')->__('Banner Image'),
            'required' => false,
            'name' => 'image',
        ));

        /*$fieldset->addField('text', 'editor', array(
            'name'      => 'text',
            'label'     => Mage::helper('slider')->__('Text'),
            'title'     => Mage::helper('slider')->__('Text'),
            'wysiwyg'   => true,
            'required'  => false,
            'config'    => $wysiwygConfig
        ));*/

        $fieldset->addField('text', 'textarea', array(
            'label' => Mage::helper('slider')->__('Text'),
            'name' => 'text',
        ));

        $fieldset->addField('sort_order', 'text', array(
            'label' => Mage::helper('slider')->__('Sort Order'),
            'name' => 'sort_order',
            'note' => 'Used for sort item in slider',
        ));

        $fieldset->addField('slider_id', 'multiselect', array(
              'name' => 'slider_id[]',
              'label' => Mage::helper('slider')->__('Slider'),
              'values'   => Mage::getModel('slider/option_slider')->getOptionArray()
        ));

        $form->setValues($data);

        return parent::_prepareForm();
    }

    protected function _prepareLayout() {
        $return = parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        return $return;
    }

}