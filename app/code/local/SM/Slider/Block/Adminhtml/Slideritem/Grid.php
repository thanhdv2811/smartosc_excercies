<?php
class SM_Slider_Block_Adminhtml_Slideritem_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('slideritemGrid');
		$this->setDefaultSort('slider_item_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}


	protected function _prepareCollection(){
		$collection = Mage::getModel('slider/slideritem')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('slider_item_id', array(
			'header'	=> Mage::helper('slider')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'slider_item_id',
		));

		$this->addColumn('image', array(
			'header'	=> Mage::helper('slider')->__('Image'),
            'width'	 => '100px',
			'align'	 =>'left',
			'index'	 => 'image',
            'renderer'  => 'slider/adminhtml_render_image'
		));

        $this->addColumn('name', array(
			'header'	=> Mage::helper('slider')->__('Name'),
			'align'	 =>'left',
			'index'	 => 'name',
		));

		$this->addColumn('status', array(
			'header'	=> Mage::helper('slider')->__('Status'),
			'align'	 => 'left',
			'width'	 => '80px',
			'index'	 => 'status',
			'type'		=> 'options',
			'options'	 => array(
				0 => 'Enabled',
				1 => 'Disabled',
			),
		));

		$this->addColumn('action',
			array(
				'header'	=>	Mage::helper('slider')->__('Action'),
				'width'		=> '100',
				'type'		=> 'action',
				'getter'	=> 'getId',
				'actions'	=> array(
					array(
						'caption'	=> Mage::helper('slider')->__('Edit'),
						'url'		=> array('base'=> '*/*/edit'),
						'field'		=> 'id'
					)),
				'filter'	=> false,
				'sortable'	=> false,
				'index'		=> 'stores',
				'is_system'	=> true,
		));


		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('slider_id');
		$this->getMassactionBlock()->setFormFieldName('slider');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('slider')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('slider')->__('Are you sure?')
		));

		return $this;
	}

	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}