<?php
class SM_FeatureProduct_Block_Categoryfeature extends Mage_Core_Block_Template{
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getFeatureProduct(){
        $_current_category = Mage::registry('current_category');
        $category_model = Mage::getModel('catalog/category')->load($_current_category->getId());
        $products = Mage::getResourceModel('catalog/product_collection')
        ->setStoreId(Mage::app()->getStore()->getId())
        ->addCategoryFilter($category_model);
        $products->addAttributeToSelect('*');
        $products->addAttributeToFilter('sm_feature', array('eq' => '1'));
        Mage::getSingleton('catalog/product_status')
	        ->addVisibleFilterToCollection($products);

        Mage::getSingleton('catalog/product_visibility')
        	->addVisibleInCatalogFilterToCollection($products);
        $products->addAttributeToSort('created_at', 'desc');
        $products->setPageSize(20);

        return $products;
    }
}