<?php
class SM_FeatureProduct_Block_Homefeature extends Mage_Core_Block_Template{
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getFeatureProduct(){
        $prodCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('sm_feature', array('eq' => '1'))
            ->addAttributeToSelect('*');
        Mage::getSingleton('catalog/product_status')
	        ->addVisibleFilterToCollection($prodCollection);

        Mage::getSingleton('catalog/product_visibility')
        	->addVisibleInCatalogFilterToCollection($prodCollection);
        $prodCollection->addAttributeToSort('created_at', 'desc');
        $prodCollection->setPageSize(20);
        $prodCollection->load();

        return $prodCollection;
    }
}