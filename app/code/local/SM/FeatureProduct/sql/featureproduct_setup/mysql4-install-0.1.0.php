<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();
$setup->addAttribute('catalog_product', 'sm_feature', array(
    'type'=>'int',
    'input'=>'select',
    'label'=>'Is Feature Product',
    "source"   => "eav/entity_attribute_source_boolean",
    'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    "visible"  => true,
    "required" => false,
    "searchable" => false,
    "filterable" => false,
    "comparable" => false
));

$setup->endSetup();
	 