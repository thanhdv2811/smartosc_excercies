<?php
class SM_FeatureProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function checkLayout(){
        $root_layout = Mage::app()->getLayout()->getBlock('root')->getTemplate();
        if(strpos($root_layout,'3columns') !== false){
            return 3;
        }elseif(strpos($root_layout,'2columns') !== false){
            return 4;
        }else{
            return 5;
        }
    }
}
	 